from rest_framework import serializers

TITLE_OPTIONS = (
            ("movie", "movie"),
            ("series", "series"),
            ("episode", "episode"),
        )


class ParamsSerializer(serializers.Serializer):
    # Title = serializers.CharField(required=False)
    title = serializers.CharField(required=False)
    year = serializers.CharField(max_length=4, min_length=4, required=False)
    imdbID = serializers.CharField(required=False)
    type = serializers.ChoiceField(
        choices=TITLE_OPTIONS,
        required=False
    )

    @property
    def params_validated(self):
        data = self.validated_data.copy()
        old_data = self.initial_data
        keys = ("title", "year", "imdbID", "type")
        first_data = {key: old_data.pop(key) for key in keys if key in data}

        return first_data, old_data
