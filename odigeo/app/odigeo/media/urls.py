from django.urls import path
from media.views import Media

urlpatterns = [
    path('v1/media/', Media.as_view()),
    path('v1/media/title/<title>', Media.as_view(), name="title"),
    path('v1/media/imdbID/<imdbID>', Media.as_view(), name="imdbID")
]
