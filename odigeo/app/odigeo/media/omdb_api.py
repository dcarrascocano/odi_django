import httpx
import environ
import requests
import asyncio


async def async_search_media(name, is_id, **params) -> dict:
    env = environ.Env()
    environ.Env.read_env()
    key = env("OMDBAPI_KEY")
    base_url = f"https://www.omdbapi.com/?apikey={key}&"
    url = f"{base_url}s={name}" if is_id is False else f"{base_url}i={name}"
    mapping = {
        "Type": "type",
        "Year": "y"
    }
    for k, v in params.items():
        url = f"{url}&{mapping.get(k)}={v}"

    try:
        async with httpx.AsyncClient() as client:
            result = await client.get(url)

        if result.status_code in range(200, 300):
            data = result.json()
        else:
            return {"message": "Unknown error"}

        if data["Response"] == "True":
            if is_id is True:
                result = {"message": "OK", "data": data}
            else:
                result = {"message": "OK", "data": data["Search"]}
            return result
        else:
            return {"message": data["Error"]}

    except Exception as e:
        return {"message": f"{e}"}


async def get_responses(ids: list):
    responses = await asyncio.gather(*(async_search_media(ID, True) for ID in ids))

    return responses


def search_media(name, is_id, **params) -> dict:
    base_url = "https://www.omdbapi.com/?apikey=9f2cc04a&"
    url = f"{base_url}s={name}" if is_id is False else f"{base_url}i={name}"
    mapping = {
        "Type": "type",
        "Year": "y"
    }
    for k, v in params.items():
        url = f"{url}&{mapping.get(k)}={v}"

    try:
        result = requests.get(url)

        if result.status_code in range(200, 300):
            data = result.json()
        else:
            return {"message": "Unknown error"}

        if data["Response"] == "True":
            if is_id is True:
                result = {"message": "OK", "data": data}
            else:
                result = {"message": "OK", "data": data["Search"]}
            return result
        else:
            return {"message": data["Error"]}

    except Exception as e:
        return {"message": f"{e}"}
