from django.shortcuts import render
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
import pandas as pd
from functools import reduce
from typing import List
from media.serializers import ParamsSerializer
from media.omdb_api import async_search_media, get_responses, search_media
import asyncio
import numpy as np


class Media(ListAPIView):
    serializer_class = ParamsSerializer

    @staticmethod
    def data_process(data, **params_search):
        df = pd.DataFrame(data)
        df = df.fillna(np.nan).replace([np.nan], [None])
        values = [(i, j) for i, j in params_search.items() if i in df.columns]
        series = pd.Series([True] * len(df))
        selection = reduce(lambda x, y: x & df[y[0]].str.contains(y[1]), values, series)
        result = [{k: v for k, v in _dict.items() if v != "N/A"} for _dict in df[selection].to_dict('records')]

        return result

    def search(self, name, is_id,  **params):
        new_params = {i: j[0] for i, j in params.items() if j[0] != ""}
        serializer = self.serializer_class(data=new_params)

        if not serializer.is_valid():
            error = "\r\n".join(["\r\n".join(value[:]) for value in serializer.errors.values()])
            return {"message": error}, {}

        params_request, params_search = serializer.params_validated
        response = search_media(name, is_id, **params_request)
        message = response.pop("message")

        if message != "OK":
            return {"message": message}, {}

        if not is_id:

            ids = [media["imdbID"] for media in response["data"]]

            loop = asyncio.new_event_loop()
            futures = get_responses(ids)
            responses = loop.run_until_complete(futures)
        else:
            responses = [response]

        pre_df = [response["data"] for response in responses]
        result = ({"message": "OK", "data": pre_df}, params_search)

        return result

    def build_request(self, params, *args, **kwargs):
        title = kwargs.get("title", False)
        id_value = kwargs.get("imdbID", False)

        if any((title, id_value)) is False:
            result = {
                    "message": "title or imdbID not set or both used in same time"
                }
            return result

        is_id, name = (True, id_value) if id_value else (False, title)
        data, params_search = self.search(name, is_id, **params)
        message = data.pop("message")

        if message != "OK":
            result = {
                    "search": params,
                    "result": message,
                }
            return result

        data = self.data_process(data["data"], **params_search)

        result = {
                "search": params,
                "result": data,
            }

        return result

    def get(self, request, format=None, *args, **kwargs):
        params = dict(request.query_params)
        kwargs_copy = kwargs.copy()
        result = self.build_request(params, **kwargs_copy)
        import pdb; pdb.set_trace()

        return Response(result)

    def post(self, request, format=None, *args, **kwargs):
        params = dict(request.data.copy())

        if bool(kwargs):
            return Response(
                {
                    "search": kwargs,
                    "message": "You have used a wrong url with query string in a POST request.\r\n"
                               "Please use query string parameters like data in POST request using this url:\r\n"
                               "http://localhost:port/media/ "
                }
            )
        kwargs_copy = {k: params.pop(k)[0] for k in ("imdbID", "title") if k in params}
        result = self.build_request(params, **kwargs_copy)

        return Response(result)

# tt0214341
