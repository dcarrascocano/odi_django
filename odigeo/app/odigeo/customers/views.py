import django.db.models
from django.shortcuts import render, HttpResponse
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from customers.models import FunnelTable, BookingTable
from datetime import date
from django.db.models import Count, Sum


def generate_table(request, *args, **kwargs):
    id_value = kwargs.get("id")
    query, query_set = queries.get(id_value)()
    list_query_set = list(query_set)
    context = {
        "columns": list_query_set[0].keys(),
        "data": [list(_dict.values()) for _dict in list_query_set],
        "id_value": id_value,
        "question": questions[id_value],
        "query": query
    }

    return render(request, "index.html", context)


queries = {
    "1": lambda: BookingTable.get_total_revenue(["country", "device"], "2019-11-2"),
    "2": lambda: FunnelTable.get_pages_by_country("sessionid"),
    "3": lambda: FunnelTable.get_sessions_seen_by("home", "payment"),
    "4": lambda: FunnelTable.get_continuance_rate_by("country", "device"),
    "5": lambda: FunnelTable.get_max_landing_page_by('sessionid'),
    "6": lambda: FunnelTable.get_conversion_rate_by(['page', 'device']),
    "7": lambda: FunnelTable.get_exit_rate_by("sessionid", 'results')
}

questions = {
    "1": "What is the total revenue generated per each country AND device the 2nd of November,2019?",
    "2": "What country has the most page views per session on average?",
    "3": "How many sessions have seen the “home” page AND the “payment” page?",
    "4": "We define “continuance rate” as the percentage of sessions that have progressed from "
         "payment to confirmation, i.e. (sessions that have seen confirmation) / (sessions that have "
         "seen payment). Compute it by country AND device.",
    "5": "We define “landing page” as the first page in a session. What is the most abundant landing "
         "page?",
    "6": "We define conversion rate as (total transactions) / (total sessions). What is the conversion "
         "rate per landing page AND device?",
    "7": "We define “Exit rate” as the percentage of times a page was the last one in a session, out of "
         "all the times the page was viewed. What is the exit rate of “results”?"
}


# Create your views here.
# FunnelTable.objects \
#     .filter(timestamp__date=date(2019, 11, 1)) \
#     .extra(
#         select={'revenue': 'booking_table.revenue'},
#         tables=['booking_table'],
#         where=['funnel_table.transactionid = booking_table.transactionid']
#     ) \
#     .values('country', 'device') \
#     .order_by('country', 'device') \
#     .annotate(total_revenue=Count('country'),)
#
# BookingTable.objects.all() \
#     .extra(
#         select={'country': 'funnel_table.country',
#                 'device': 'funnel_table.device',
#                 'timestamp': 'funnel_table.timestamp'
#                 },
#         tables=['funnel_table'],
#         where=["funnel_table.transactionid = booking_table.transactionid",
#                "DATE(funnel_table.timestamp) = '2019-11-2'"],
#     ) \
#     .values('country', 'device') \
#     .annotate(total_revenue=Sum('revenue')) \
#     .order_by()
