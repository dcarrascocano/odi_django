from decimal import Decimal
from functools import reduce
from time import time
from MySQLdb import Timestamp

from django.db import models
from django.db.models import Count, Sum, Q, F, Min, Value, TextField, Max
from django.db.models.functions import Concat

from datetime import datetime as dt
import uuid
from django.forms import CharField


def replace_with_quotes(old_str, *values):
    return reduce(lambda x, y: x.replace(y, f"'{y}'"), values, old_str)


class BookingTable(models.Model):
    class Meta:
        db_table = "booking_table"

    transactionid = models.IntegerField(
        unique=True,
        primary_key=True
    )
    product = models.CharField(
        max_length=50,
    )
    revenue = models.FloatField()

    @classmethod
    def get_total_revenue(cls, columns: list, date_value: str):
        select = {column: f"funnel_table.{column}" for column in columns}
        result = cls.objects.all() \
            .extra(
            select={**select,
                    'timestamp': 'funnel_table.timestamp'
                    },
            tables=['funnel_table'],
            where=["funnel_table.transactionid = booking_table.transactionid",
                   f"DATE(funnel_table.timestamp) = '{date_value}'"],
        ) \
            .values(*columns) \
            .annotate(total_revenue=Sum('revenue')) \
            .order_by()

        return str(result.query), result


class FunnelTable(models.Model):
    class Meta:
        db_table = "funnel_table"

    DEVICE_CHOICES = (
        ("mobile", "mobile"),
        ("desktop", "desktop"),
        ("table", "table"),
    )
    userid = models.CharField(
        max_length=50,
    )
    # This field should be models.UUIDField but due in the data provided are integer isn't possible
    # sessionid = models.UUIDField(
    #     #     unique=False,
    #     #     editable=False,
    #     #     default=uuid.uuid4,
    #     #     verbose_name='Public identifier',
    #     # )
    sessionid = models.IntegerField()
    timestamp = models.DateTimeField()
    page = models.CharField(
        max_length=50,
    )
    channel = models.CharField(
        max_length=50,
    )
    device = models.CharField(
        max_length=50,
        choices=DEVICE_CHOICES,
    )
    browser = models.CharField(
        max_length=50,
    )
    country = models.CharField(
        max_length=50,
    )
    # This field should Autofield but due in the data provided are null or empty values isn't possible
    transactionid = models.IntegerField(
        null=True,
        blank=True
    )

    @classmethod
    def get_pages_by_country(cls, column: str):
        result = cls.objects.all() \
            .values('country') \
            .annotate(
            average=Count('page') / ((Count(column, distinct=True)) * 1.0),
        ) \
            .order_by('-average')

        return str(result.query), result[0:1]

    @classmethod
    def get_sessions_seen_by(cls, *pages):
        condition = reduce(lambda x, y: x | Q(page=y), [Q(page=pages[0]), *pages[1::]])

        result = cls.objects \
            .filter(condition) \
            .values("page") \
            .order_by() \
            .annotate(
                total=Count('sessionid', distinct=True)
            )

        old_query = str(result.query)
        query = replace_with_quotes(old_query, *pages)

    # result = result.aggregate

        return query, result

    @classmethod
    def get_continuance_rate_by(cls, *fields):
        condition = Q(page="payment") | Q(page="confirmation")
        continuance_rate = (
                Count('sessionid', filter=Q(page="payment")) /
                Count('sessionid', filter=Q(page="confirmation")) * 1.0
        )

        result = cls.objects \
            .filter(condition) \
            .values(*fields) \
            .order_by() \
            .annotate(
                continuance_rate=continuance_rate,
            )

        old_query = str(result.query)
        query = replace_with_quotes(old_query, "payment", "confirmation")

        return query, result

    @classmethod
    def get_page_in_position_by(cls, position: str, field: str):
        condition = Min('timestamp') if position == "first" else Max('timestamp')
        pre_result = cls.objects \
            .values(field) \
            .order_by(field) \
            .annotate(
                count=Count('page'),
                date_value=condition,
                mix=Concat(field, Value("-"), "date_value", output_field=TextField())
            ) \
            .values("mix")

        result = cls.objects \
            .annotate(
                mix=Concat('sessionid', Value("-"), "timestamp", output_field=TextField()),
            ) \
            .filter(mix__in=pre_result)

        return str(result.query), result

    @classmethod
    def get_max_landing_page_by(cls, field: str):
        pre_result = cls.get_page_in_position_by("first", field)[1]
        result = pre_result.values('page') \
            .order_by() \
            .annotate(
            count=Count('page')
        ) \
            .order_by('-count')

        return str(result.query), result

    @classmethod
    def get_conversion_rate_by(cls, fields: list):
        pre_result = cls.get_page_in_position_by("first", 'sessionid')[1]
        result = pre_result \
            .values(*fields) \
            .annotate(
                rate=Count('transactionid') / ((Count('sessionid')) * 1.0),
            ) \
            .order_by()

        return str(result.query), result

    @classmethod
    def get_exit_rate_by(cls, field: str, page_value: str):
        pre_result = cls.get_page_in_position_by("last", field)[1]
        result = pre_result \
            .values('page') \
            .annotate(
                count=Count(field)
            ) \
            .order_by('count')

        old_query = str(result.query)
        result = pre_result \
            .values('page') \
            .aggregate(
                rate=Count(field,
                           filter=Q(page=page_value)) / Count(field) * 1.0,
            )
        query = old_query\
            .replace(
                "CONCAT_WS('', `funnel_table`.`sessionid`, CONCAT_WS('', -, `funnel_table`.`timestamp`)) ",
                "CONCAT_WS('-', `funnel_table`.`sessionid`, `funnel_table`.`timestamp`) "
            ) \
            .replace(
                "CONCAT_WS('', U0.`sessionid`, CONCAT_WS('', -, MAX(U0.`timestamp`))",
                "CONCAT_WS('-', U0.`sessionid`, MAX(U0.`timestamp`))"
            )
        return query, (result,)
