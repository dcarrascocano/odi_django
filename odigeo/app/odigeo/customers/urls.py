from django.urls import path
from customers.views import generate_table

urlpatterns = [
    # path('query/<id>', Customer.as_view(), name="customer"),
    path('query/<id>', generate_table, name="table"),
]
